﻿
using Newtonsoft.Json.Linq;
using NUnit.Framework;

namespace Coscine.NotificationConfiguration.Test
{
    [TestFixture]
    public class NotificationConfigurationTest
    {
        private static readonly string _actionNameValid = "project_deleted";
        private static readonly string _actionNameInvalid = "hfauifhsdksndah";

        private static readonly string _channelNameValid = "email";
        private static readonly string _channelNameInvalid = "hfauifhsdksndah";

        private static readonly string _languageValid = "en";

        public NotificationConfigurationTest()
        {
        }

        [OneTimeSetUp]
        public void Setup()
        {

        }

        [OneTimeTearDown]
        public void End()
        {
        }

        [Test]
        public void TestConstructor()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
        }

        [Test]
        public void TestIsValidAction()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.IsValidAction(_actionNameValid));
            Assert.False(notificationConfiguration.IsValidAction(_actionNameInvalid));
        }

        [Test]
        public void TestIsValidChannel()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.IsValidChannel(_channelNameValid));
            Assert.False(notificationConfiguration.IsValidChannel(_channelNameInvalid));
        }

        [Test]
        public void TestGetAction()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.GetAction(_actionNameValid).GetType() == new ActionObject().GetType());
            Assert.True(notificationConfiguration.GetAction(_actionNameInvalid) == null);
        }

        [Test]
        public void TestGetChannel()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.GetChannel(_channelNameValid).GetType() == new ChannelObject().GetType());
            Assert.True(notificationConfiguration.GetChannel(_channelNameInvalid) == null);
        }

        [Test]
        public void TestGetAllChannelsForAction()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.GetAllChannelsForAction(_actionNameValid).Count > 0);
            Assert.True(notificationConfiguration.GetAllChannelsForAction(_actionNameInvalid).Count == 0);
        }

        [Test]
        public void TestGetAllPartialsForChannel()
        {
            Coscine.NotificationConfiguration.NotificationConfiguration notificationConfiguration = new Coscine.NotificationConfiguration.NotificationConfiguration();
            Assert.True(notificationConfiguration.GetPartialsForChannel(_channelNameValid, _languageValid).Count > 0);
        }
    }
}
