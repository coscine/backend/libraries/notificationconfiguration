﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Coscine.NotificationConfiguration
{
    public class NotificationConfiguration
    {
        private readonly Dictionary<string, ActionObject> _actions = new Dictionary<string, ActionObject>();

        private readonly Dictionary<string, ChannelObject> _channels = new Dictionary<string, ChannelObject>();

        public NotificationConfiguration()
        {
            var actionsFileName = "Coscine.NotificationConfiguration.Actions.json";
            var channelsFileName = "Coscine.NotificationConfiguration.Channels.json";

            var actionsJSON = GetJSONFromAssembly(actionsFileName);
            var channelsJSON = GetJSONFromAssembly(channelsFileName);

            foreach (var action in actionsJSON)
            {
                _actions.Add(action.Key, new ActionObject((JObject)action.Value));
            }
            foreach (var channel in channelsJSON)
            {
                _channels.Add(channel.Key, new ChannelObject((JObject)channel.Value));
            }
        }

        public static JObject GetJSONFromAssembly(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(fileName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string result = reader.ReadToEnd();
                    return (JObject)JsonConvert.DeserializeObject(result);
                }
            }
        }

        public static string GetStringFromAssembly(string fileName)
        {
            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(fileName))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static string GetStringFromAssembly(string channel, string language, string fileName)
        {
            var path = $"Coscine.NotificationConfiguration.Partials.{UppercaseFirst(channel)}.{UppercaseFirst(language)}.{fileName}";
            return GetStringFromAssembly(path);
        }

        public bool IsValidAction(string action)
        {
            return _actions.ContainsKey(action);
        }

        public bool IsValidChannel(string channel)
        {
            return _channels.ContainsKey(channel);
        }

        public ActionObject GetAction(string action)
        {
            if (_actions.ContainsKey(action))
            {
                return _actions[action];
            }
            return null;
        }

        public ChannelObject GetChannel(string channel)
        {
            if (_channels.ContainsKey(channel))
            {
                return _channels[channel];
            }
            return null;
        }
        public Dictionary<string, string> GetPartialsForChannel(string channel, string language)
        {
            if (_channels.ContainsKey(channel))
            {
                var args = _channels[channel].Args;
                var dict = new Dictionary<string, string>();
                if (args["partials"] != null && args["partials"][language] != null)
                {
                    var partialDict = args["partials"][language].ToObject<Dictionary<string, string>>();
                    foreach (var partial in partialDict)
                    {
                        dict.Add(partial.Key, GetStringFromAssembly(channel, language, partial.Value));
                    }
                }
                return dict;
            }
            return null;
        }

        private static string UppercaseFirst(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            return char.ToUpper(str[0]) + str.Substring(1);
        }

        public List<string> GetAllChannelsForAction(string action)
        {
            ActionObject actionObject = GetAction(action);
            if (actionObject != null)
            {
                var only = actionObject.ChannelsOnly;
                var available = actionObject.ChannelsAvailable;
                var always = actionObject.ChannelsOnly;
                if (only.Count > 0)
                {
                    return only;
                }
                else
                {
                    return available.Union(always).ToList();
                }
            }
            return new List<string>();
        }
    }
}
