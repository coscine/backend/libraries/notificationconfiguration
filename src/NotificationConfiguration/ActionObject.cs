﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.NotificationConfiguration
{
    public class ActionObject
    {

        public JObject Template { get; set; }

        public Dictionary<string, string> Name { get; set; }

        public Dictionary<string, string> Description { get; set; }

        public List<string> Group { get; set; }

        public List<string> ChannelsAvailable { get; set; }

        public List<string> ChannelsAlways { get; set; }

        public List<string> ChannelsOnly { get; set; }

        public ActionObject()
        {

        }

        public ActionObject(JObject template, Dictionary<string, string> name, Dictionary<string, string> description, List<string> group, List<string> channelsAvailable, List<string> channelsAlways, List<string> channelsOnly)
        {
            Template = template;
            Name = name;
            Description = description;
            Group = group;
            ChannelsAvailable = channelsAvailable;
            ChannelsAlways = channelsAlways;
            ChannelsOnly = channelsOnly;
        }

        public ActionObject(JObject channelData)
        {
            Template = (JObject)channelData["template"];
            Name = channelData["name"].ToObject<Dictionary<string, string>>();
            Description = channelData["name"].ToObject<Dictionary<string, string>>();
            Group = channelData["group"].ToObject<List<string>>();
            ChannelsAvailable = channelData["channels"]["available"].ToObject<List<string>>();
            ChannelsAlways = channelData["channels"]["always"].ToObject<List<string>>();
            ChannelsOnly = channelData["channels"]["only"].ToObject<List<string>>();
        }
    }
}
