﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Coscine.NotificationConfiguration
{
    public class ChannelObject
    {
        public string Path { get; set; }
        public JObject Args { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Description { get; set; }

        public ChannelObject()
        {

        }

        public ChannelObject(string path, JObject args, Dictionary<string, string> name, Dictionary<string, string> description)
        {
            Path = path;
            Args = args;
            Name = name;
            Description = description;
        }

        public ChannelObject(JObject channelData)
        {
            Path = channelData["path"].ToString();
            Args = (JObject)channelData["args"];
            Name = channelData["name"].ToObject<Dictionary<string, string>>();
            Description = channelData["name"].ToObject<Dictionary<string, string>>();
        }

    }
}
